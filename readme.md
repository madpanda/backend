# Dev branch for RIPR
Some description about our awesome project.

## About Node-Gyp
Node-gyp is used to compile and link C/C++ code to NodeJS. In the binding.gyp file, an action for copying build to /dist/build is described. Please note that on linux, a different path is used for copying files. 


Linux:
```
{
    "targets": [
            ...
            "target_name": "CopyBuildToDist",
            ...
            "actions": [
                {
                    "action_name": "CopyBuildToDist",
                    ...
                    "action": [
                        "cp -rf ./build ./dist/build"
                    ]
                }
            ]
        }
    ]
}
```

Windows:
``` unknown ```

