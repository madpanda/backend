import * as fs from "fs"
import { Logger } from "./Modules/Logging/Logger";

export class Settings {
    private static RawSettings: any = null;
    public static GetSettings(name: string): any {
        const settingsPath = "config.json";

        if (!Settings.RawSettings) {
            if (fs.existsSync(settingsPath)) {
                Settings.RawSettings = JSON.parse(fs.readFileSync(settingsPath).toString());
            } else {
                Settings.RawSettings = {
                    "HID_VALID_CARD": "6aa8",
                    "PATH_TO_PROXMARK_APP": "/home/pi/Downloads/proxmark3/client/proxmark3",
                    "PATH_TO_PROXMARK_TTY": "/dev/ttyACM0",
                    "PATH_TO_PROXMARK_TTY_FALLBACK": "/dev/ttyACM1",
                    "DEBUG": true
                }

                fs.writeFileSync(settingsPath, JSON.stringify(Settings.RawSettings));
            }
        }

        var val = Settings.RawSettings[name];

        if(!val) {
            Logger.LogWarn("Cannot find Setting: " + name);
        }

        return val;
    }
}
