import * as os from "os";

import * as child_process from "child_process";
import { Logger } from "../Logging/Logger";

var cmd = os.platform() == "win32"
    ? "shutdown -s -t 0"
    : "sudo shutdown -h now";

export class ShutdownModule {
    private static Execute(command, callback){
        child_process.exec(command, function(error, stdout, stderr){ callback(stdout); });
    }

    public static Shutdown(): void {
        ShutdownModule.Execute(cmd, () => {
            Logger.LogInfo("Shutting down...");
        });
    }
}
