import * as crypto from "crypto";
var Prompt = require('prompt-password');

type cryptoCallback = (s: string) => void;

export class ConsoleAuth {

    private hashedVariable: crypto.Hash;
    private hashingAlgorithm: string;

    constructor() {
        this.hashingAlgorithm = "sha256"; //TODO: place in config?
        this.hashedVariable = crypto.createHash(this.hashingAlgorithm);
    }

    public Authenticate(description: string, callback: cryptoCallback, encoding: crypto.HexBase64Latin1Encoding = "base64"): void {
        var prompt = new Prompt({
            type: 'password',
            message: description,
            name: 'password'
        });

        prompt.run()
            .then((answer) => {
                this.hashedVariable.update(answer);
                callback(this.hashedVariable.digest(encoding));
            });
    };
}