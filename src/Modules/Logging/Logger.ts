import * as winston from "winston";
import util = require("util");

export namespace Logger {
    const logger = new winston.Logger({
        transports: [
            new winston.transports.Console({
                colorize: true,
                timestamp: true,
                handleExceptions: true,
                humanReadableUnhandledException: true,
                prettyPrint: true,
            })
        ],
        exitOnError: false,
    });

    export function LogError(...params:string[]): void {
        Logger.Log("error", params);
    }

    export function LogWarn(...params:string[]): void {
        Logger.Log("warn", params);
    }

    export function LogInfo(...params:string[]): void {
        Logger.Log("info", params);
    }

    export function LogDebug(...params:string[]): void {
        Logger.Log("debug", params);
    }

    export function Log(level: string, params:string[]): void {
        logger.log(level, params.join(' '));
    }
}
