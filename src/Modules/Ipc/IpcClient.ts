import * as ipc from "node-ipc";
import { Logger } from "../Logging/Logger";

ipc.config.retry = 1500;
ipc.config.silent = true;

export class IpcClient {
    static Connect(channel: string, clientId: string, callbacks: { [key: string]: ((ipc, data, send: ((key: string, value?: any) => void)) => void) }): ((key: string, value: string) => void) {
        ipc.config.id = clientId;

        ipc.connectTo(channel, () => {
            //Connect
            ipc.of[channel].on("connect", () => {
                ipc.of[channel].emit("message", {
                    key: "connect"
                });

                if(callbacks["connect"]) {
                    callbacks["connect"](ipc, null, (key, value) => {
                        ipc.of[channel].emit("message", {
                            key: key,
                            value: value
                        });
                    });
                }
            });

            //Disconnect
            ipc.of[channel].on("disconnect", () => {
                if(callbacks["disconnect"]) callbacks["disconnect"](ipc, null, null);
            });

            //Message
            ipc.of[channel].on("message", (data) => {
                if(data.key) {
                    if(callbacks[data.key]) {
                        callbacks[data.key](ipc, data.value, (key, value) => {
                            ipc.of[channel].emit("message", {
                                key: key,
                                value: value
                            });
                        });
                    }
                } else {
                    console.log("Client Invalid Message");
                }
            });
        });

        return ((key, value) => {
            ipc.of[channel].emit("message", {
                key: key,
                value: value
            });
        });
    }
}