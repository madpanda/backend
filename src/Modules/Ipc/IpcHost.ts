import { IPC } from "node-ipc";
import { Socket } from "net";

export class IpcHost {
    static Serve(name: string, callbacks: { [key: string]: ((ipc, data, socket, send: ((key: string, value?: any) => void)) => void) }): void {
        var ipc = new IPC();

        ipc.config.retry = 1500;
        ipc.config.silent = true;
        ipc.config.id = name;

        ipc.serve(() => {
            //Message
            ipc.server.on("message", (data, socket: Socket) => {
                if(data.key) {
                    if(callbacks[data.key]) {
                        callbacks[data.key](ipc, data.value, socket, (key, value) => {
                            ipc.server.emit(socket, "message", {
                                key: key,
                                value: value
                            });
                        });
                    }
                } else {
                    console.log("Host Invalid Message");
                }
            });

            //Disconnect
            ipc.server.on("socket.disconnected", (data, socket) => {
                if(callbacks["disconnect"]) callbacks["disconnect"](ipc, data, socket, null);
            });
        });

        ipc.server.start();
    }
}