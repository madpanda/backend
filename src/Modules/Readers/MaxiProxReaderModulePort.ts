import { INPUT, INT_EDGE_FALLING } from "wiring-pi";
import * as wiringPi from "wiring-pi"
import { usleep } from "sleep"
//Definitions
export const D0_PIN = 0
export const D1_PIN = 1

export const WIEGANDMAXDATA = 100
export const WIEGANDTIMEOUT = 5000000

export const bitRead = (value, bit) => (((value) >> (bit)) & 0x01)
export const bitSet = (value, bit) => ((value) |= (1 << (bit)))
export const bitClear = (value, bit) => ((value) &= ~(1 << (bit)))
export const bitWrite = (value, bit, bitvalue) => (bitvalue ? bitSet(value, bit) : bitClear(value, bit))

class timespec {
    public tv_sec: number;
    public tv_nsec: number;

    constructor(old?: [number, number]) {
        if (old) {
            this.tv_sec = old[0];
            this.tv_nsec = old[1];
        }
        else {
            var tv = process.hrtime();
            this.tv_sec = tv[0];
            this.tv_nsec = tv[1];
        }
    }

    public deltaNow(): timespec {
        return new timespec(process.hrtime([this.tv_sec, this.tv_nsec]));
    }
}

export var __wiegandData: number[] = new Array<number>(WIEGANDMAXDATA);
export var __wiegandBitCount: number;
export var __wiegandBitTime: timespec = new timespec();
export var flagDone: boolean;
export var weigand_counter: number;
export var facilityCode: number = 0;
export var cardCode: number = 0;
export var bitHolder1: number = 0;
export var bitHolder2: number = 0;
export var cardChunk1: number = 0;
export var cardChunk2: number = 0;

export function data0Pulse() {
    __wiegandBitCount++;
    flagDone = false;

    if (__wiegandBitCount < 23) {
        bitHolder1 = bitHolder1 << 1;
    }
    else {
        bitHolder2 = bitHolder2 << 1;
    }

    weigand_counter = WIEGANDTIMEOUT;
}

export function data1Pulse() {
    __wiegandData[__wiegandBitCount] = 1;
    __wiegandBitCount++;
    flagDone = false;

    if (__wiegandBitCount < 23) {
        bitHolder1 = bitHolder1 << 1;
        bitHolder1 |= 1;
    }
    else {
        bitHolder2 = bitHolder2 << 1;
        bitHolder2 |= 1;
    }

    weigand_counter = WIEGANDTIMEOUT;
}

export function wiegandReset() {
    __wiegandData = __wiegandData.map(x => x = 0);
}


export function wiegandInit(d0pin: number, d1pin: number) {
    wiringPi.wiringPiSetup();
    wiringPi.pinMode(d0pin, INPUT);
    wiringPi.pinMode(d1pin, INPUT);

    wiringPi.wiringPiISR(d0pin, INT_EDGE_FALLING, data0Pulse);
    wiringPi.wiringPiISR(d1pin, INT_EDGE_FALLING, data1Pulse);
}


export function wiegandGetPendingBitCount() {

    var delta: timespec = __wiegandBitTime.deltaNow();

    if ((delta.tv_sec > 1) || (delta.tv_nsec > WIEGANDTIMEOUT))
        return __wiegandBitCount;

    return 0;
}


export function getCardNumAndSiteCode() // copy paste from arduino code. modified to take "correct" vars (__wiegandBitCount and __wiegandData)
{
    var i: number = 0;

    // we will decode the bits differently depending on how many bits we have
    // see www.pagemac.com/azure/data_formats.php for more info
    // also specifically: www.brivo.com/app/static_data/js/calculate.js
    switch (__wiegandBitCount) {

        ///////////////////////////////////////
        // standard 26 bit format
        // facility code = bits 2 to 9
        case 26:
            for (i = 1; i < 9; i++) {
                facilityCode <<= 1;
                facilityCode |= __wiegandData[i];
            }

            // card code = bits 10 to 23
            for (i = 9; i < 25; i++) {
                cardCode <<= 1;
                cardCode |= __wiegandData[i];
            }
            break;

        ///////////////////////////////////////
        // 33 bit HID Generic
        case 33:
            for (i = 1; i < 8; i++) {
                facilityCode <<= 1;
                facilityCode |= __wiegandData[i];
            }

            // card code
            for (i = 8; i < 32; i++) {
                cardCode <<= 1;
                cardCode |= __wiegandData[i];
            }
            break;

        ///////////////////////////////////////
        // 34 bit HID Generic
        case 34:
            for (i = 1; i < 17; i++) {
                facilityCode <<= 1;
                facilityCode |= __wiegandData[i];
            }

            // card code
            for (i = 17; i < 33; i++) {
                cardCode <<= 1;
                cardCode |= __wiegandData[i];
            }
            break;

        ///////////////////////////////////////
        // 35 bit HID Corporate 1000 format
        // facility code = bits 2 to 14
        case 35:
            for (i = 2; i < 14; i++) {
                facilityCode <<= 1;
                facilityCode |= __wiegandData[i];
            }

            // card code = bits 15 to 34
            for (i = 14; i < 34; i++) {
                cardCode <<= 1;
                cardCode |= __wiegandData[i];
            }
            break;
    }
    return;
}


export function getCardValues() {
    var i: number = 0;
    switch (__wiegandBitCount) {
        case 26:
            // Example of full card value
            // |>   preamble   <| |>   Actual card value   <|
            // 000000100000000001 11 111000100000100100111000
            // |> write to chunk1 <| |>  write to chunk2   <|

            for (i = 19; i >= 0; i--) {
                if (i == 13 || i == 2) {
                    bitWrite(cardChunk1, i, 1); // Write preamble 1's to the 13th and 2nd bits
                }
                else if (i > 2) {
                    bitWrite(cardChunk1, i, 0); // Write preamble 0's to all other bits above 1
                }
                else {
                    bitWrite(cardChunk1, i, bitRead(bitHolder1, i + 20)); // Write remaining bits to cardChunk1 from bitHolder1
                }
                if (i < 20) {
                    bitWrite(cardChunk2, i + 4, bitRead(bitHolder1, i)); // Write the remaining bits of bitHolder1 to cardChunk2
                }
                if (i < 4) {
                    bitWrite(cardChunk2, i, bitRead(bitHolder2, i)); // Write the remaining bit of cardChunk2 with bitHolder2 bits
                }
            }
            break;

        case 27:
            for (i = 19; i >= 0; i--) {
                if (i == 13 || i == 3) {
                    bitWrite(cardChunk1, i, 1);
                }
                else if (i > 3) {
                    bitWrite(cardChunk1, i, 0);
                }
                else {
                    bitWrite(cardChunk1, i, bitRead(bitHolder1, i + 19));
                }
                if (i < 19) {
                    bitWrite(cardChunk2, i + 5, bitRead(bitHolder1, i));
                }
                if (i < 5) {
                    bitWrite(cardChunk2, i, bitRead(bitHolder2, i));
                }
            }
            break;

        case 28:
            for (i = 19; i >= 0; i--) {
                if (i == 13 || i == 4) {
                    bitWrite(cardChunk1, i, 1);
                }
                else if (i > 4) {
                    bitWrite(cardChunk1, i, 0);
                }
                else {
                    bitWrite(cardChunk1, i, bitRead(bitHolder1, i + 18));
                }
                if (i < 18) {
                    bitWrite(cardChunk2, i + 6, bitRead(bitHolder1, i));
                }
                if (i < 6) {
                    bitWrite(cardChunk2, i, bitRead(bitHolder2, i));
                }
            }
            break;

        case 29:
            for (i = 19; i >= 0; i--) {
                if (i == 13 || i == 5) {
                    bitWrite(cardChunk1, i, 1);
                }
                else if (i > 5) {
                    bitWrite(cardChunk1, i, 0);
                }
                else {
                    bitWrite(cardChunk1, i, bitRead(bitHolder1, i + 17));
                }
                if (i < 17) {
                    bitWrite(cardChunk2, i + 7, bitRead(bitHolder1, i));
                }
                if (i < 7) {
                    bitWrite(cardChunk2, i, bitRead(bitHolder2, i));
                }
            }
            break;

        case 30:
            for (i = 19; i >= 0; i--) {
                if (i == 13 || i == 6) {
                    bitWrite(cardChunk1, i, 1);
                }
                else if (i > 6) {
                    bitWrite(cardChunk1, i, 0);
                }
                else {
                    bitWrite(cardChunk1, i, bitRead(bitHolder1, i + 16));
                }
                if (i < 16) {
                    bitWrite(cardChunk2, i + 8, bitRead(bitHolder1, i));
                }
                if (i < 8) {
                    bitWrite(cardChunk2, i, bitRead(bitHolder2, i));
                }
            }
            break;

        case 31:
            for (i = 19; i >= 0; i--) {
                if (i == 13 || i == 7) {
                    bitWrite(cardChunk1, i, 1);
                }
                else if (i > 7) {
                    bitWrite(cardChunk1, i, 0);
                }
                else {
                    bitWrite(cardChunk1, i, bitRead(bitHolder1, i + 15));
                }
                if (i < 15) {
                    bitWrite(cardChunk2, i + 9, bitRead(bitHolder1, i));
                }
                if (i < 9) {
                    bitWrite(cardChunk2, i, bitRead(bitHolder2, i));
                }
            }
            break;

        case 32:
            for (i = 19; i >= 0; i--) {
                if (i == 13 || i == 8) {
                    bitWrite(cardChunk1, i, 1);
                }
                else if (i > 8) {
                    bitWrite(cardChunk1, i, 0);
                }
                else {
                    bitWrite(cardChunk1, i, bitRead(bitHolder1, i + 14));
                }
                if (i < 14) {
                    bitWrite(cardChunk2, i + 10, bitRead(bitHolder1, i));
                }
                if (i < 10) {
                    bitWrite(cardChunk2, i, bitRead(bitHolder2, i));
                }
            }
            break;

        case 33:
            for (i = 19; i >= 0; i--) {
                if (i == 13 || i == 9) {
                    bitWrite(cardChunk1, i, 1);
                }
                else if (i > 9) {
                    bitWrite(cardChunk1, i, 0);
                }
                else {
                    bitWrite(cardChunk1, i, bitRead(bitHolder1, i + 13));
                }
                if (i < 13) {
                    bitWrite(cardChunk2, i + 11, bitRead(bitHolder1, i));
                }
                if (i < 11) {
                    bitWrite(cardChunk2, i, bitRead(bitHolder2, i));
                }
            }
            break;

        case 34:
            for (i = 19; i >= 0; i--) {
                if (i == 13 || i == 10) {
                    bitWrite(cardChunk1, i, 1);
                }
                else if (i > 10) {
                    bitWrite(cardChunk1, i, 0);
                }
                else {
                    bitWrite(cardChunk1, i, bitRead(bitHolder1, i + 12));
                }
                if (i < 12) {
                    bitWrite(cardChunk2, i + 12, bitRead(bitHolder1, i));
                }
                if (i < 12) {
                    bitWrite(cardChunk2, i, bitRead(bitHolder2, i));
                }
            }
            break;

        case 35:
            for (i = 19; i >= 0; i--) {
                if (i == 13 || i == 11) {
                    bitWrite(cardChunk1, i, 1);
                }
                else if (i > 11) {
                    bitWrite(cardChunk1, i, 0);
                }
                else {
                    bitWrite(cardChunk1, i, bitRead(bitHolder1, i + 11));
                }
                if (i < 11) {
                    bitWrite(cardChunk2, i + 13, bitRead(bitHolder1, i));
                }
                if (i < 13) {
                    bitWrite(cardChunk2, i, bitRead(bitHolder2, i));
                }
            }
            break;

        case 36:
            for (i = 19; i >= 0; i--) {
                if (i == 13 || i == 12) {
                    bitWrite(cardChunk1, i, 1);
                }
                else if (i > 12) {
                    bitWrite(cardChunk1, i, 0);
                }
                else {
                    bitWrite(cardChunk1, i, bitRead(bitHolder1, i + 10));
                }
                if (i < 10) {
                    bitWrite(cardChunk2, i + 14, bitRead(bitHolder1, i));
                }
                if (i < 14) {
                    bitWrite(cardChunk2, i, bitRead(bitHolder2, i));
                }
            }
            break;

        case 37:
            for (i = 19; i >= 0; i--) {
                if (i == 13) {
                    bitWrite(cardChunk1, i, 0);
                }
                else {
                    bitWrite(cardChunk1, i, bitRead(bitHolder1, i + 9));
                }
                if (i < 9) {
                    bitWrite(cardChunk2, i + 15, bitRead(bitHolder1, i));
                }
                if (i < 15) {
                    bitWrite(cardChunk2, i, bitRead(bitHolder2, i));
                }
            }
            break;
    }
    return;
}

/*
 * wiegandReadData is a simple, non-blocking method to retrieve the last code
 * processed by the API.
 * data : is a pointer to a block of memory where the decoded data will be stored.
 * dataMaxLen : is the maximum number of -bytes- that can be read and stored in data.
 * Result : returns the number of -bits- in the current message, 0 if there is no
 * data available to be read, or -1 if there was an error.
 * Notes : this function clears the read data when called. On subsequent calls,
 * without subsequent data, this will return 0.
 */

export function wiegandReadData(data: number[], dataMaxLen: number) {
    if (wiegandGetPendingBitCount() > 0) {
        var bitCount = __wiegandBitCount;
        //memcpy(data, (void *)__wiegandData, ((bitCount > dataMaxLen) ? dataMaxLen : bitCount));
        for (let i = 0; i < ((bitCount > dataMaxLen) ? dataMaxLen : bitCount); i++) {
            data[i] = __wiegandData[i];
        }
        getCardNumAndSiteCode();
        getCardValues();
        wiegandReset();
        return bitCount;
    }
    return 0;
}

// Function for masking chunk1 for block 7 generation
export function maskMagicChunk1(bitLen: number, chunk1: number): number {
    //console.log("-- Starting maskMagic --\n");
    // Mask chunk 1
    var maskChunk1: number = chunk1 & 0xFFFFF; // 20 bits
    //console.log("-- maskChunk1: %x\n", maskChunk1);
    // Create no so big mask
    var maskCount: number = (bitLen - 24) / 4;
    if ((bitLen - 24) % 4 != 0) {
        maskCount++;
    }
    //console.log("-- maskCount: %d\n", maskCount);
    var mask = 0xF;
    var i;
    for (i = 1; i < maskCount; i++) {
        mask <<= 4;
        mask += 0xF;
    }
    //console.log("-- mask: %x\n", mask);
    // Mask full value
    var maskedCombinedChunk: number = maskChunk1 & mask;
    //console.log("-- maskedCombinedChunk: %x\n", maskedCombinedChunk);
    // Return masked value
    return maskedCombinedChunk;
}

export function maxiproxLoopNode(callback: (a, b, c, d, e, f) => void) {
    wiegandInit(D0_PIN, D1_PIN);
    while (1) {
        if (!flagDone) {

            if (--weigand_counter == 0)
                flagDone = true;
        }
        // if we have bits and we the weigand counter went out
        if (__wiegandBitCount > 0 && flagDone) {
            var bitLen = wiegandGetPendingBitCount();
            if (bitLen == 0) {
                usleep(5000);
            }
            else {
                var i;
                var data = new Array<number>(100);
                bitLen = wiegandReadData(data, 100);

                var now = (new Date(Date.now()))
                //console.log("Time: ", now);
                //console.log("Bit#:%i  ", bitLen);
                //console.log("FC:%lu  ", facilityCode);
                //console.log("ID:%lu  ", cardCode);
                //console.log("Hex:%lx%lx  ", cardChunk1, cardChunk2);
                //console.log("Blk7:%010x%06x  ", maskMagicChunk1(bitLen, cardChunk1), cardChunk2);
                //console.log("Bits: ");
                callback(now, bitLen, facilityCode, cardCode, cardChunk1, cardChunk2);

                for (i = 19; i >= 0; i--) {
                    console.log("%d", bitRead(cardChunk1, i));
                }
                for (i = 23; i >= 0; i--) {
                    console.log("%d", bitRead(cardChunk2, i));
                }
                console.log("\n");
                facilityCode = 0;
                cardCode = 0;
                bitHolder1 = 0;
                bitHolder2 = 0;
                cardChunk1 = 0;
                cardChunk2 = 0;
            }
        }
    }
}