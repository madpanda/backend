import { execFile } from "child_process";
import { Events } from "../../Events";
import RFIDTag from "../../Models/RFID/RFIDTag";
import { Logger } from "../Logging/Logger";
import { Socket } from 'net';

export class MaxiProxReader {
    private debug: boolean = true;
    private active: boolean = false;
    private client: Socket;
    private static mpr: MaxiProxReader;

    public static Singleton(setAsActive: boolean = true) : MaxiProxReader{
        if (!MaxiProxReader.mpr)
            MaxiProxReader.mpr = new MaxiProxReader();
        MaxiProxReader.mpr.SetActive(setAsActive);
        return MaxiProxReader.mpr;
    }

    public static Close() {
        if (MaxiProxReader.mpr) {
            if (MaxiProxReader.mpr.client)
                MaxiProxReader.mpr.client.end();
            Events.onRFIDRead.detach(MaxiProxReader.OnDataRead);
            Events.onSetReaderOnOffState.detach(MaxiProxReader.mpr.SetActive);
            if (MaxiProxReader.mpr.client && !MaxiProxReader.mpr.client.destroyed)
                MaxiProxReader.mpr.client.destroy();
            MaxiProxReader.mpr = null;
        }
    }

    private constructor() {
		execFile("/home/pi/projects/MaxiProx-RFID/bin/ARM/Debug/MaxiProx-RFID.out",[], (error, stdout, stderr) => {
            if (error) {
                console.error(`exec error: ${error}`);
				return;
            }
			
			
            console.log(`maxiprox server stdout: ${stdout}`);
            console.log(`MaxiProx Server stderr: ${stderr}`);
		});
		
		this.client = new Socket()
			.connect(5884, '127.0.0.1', () => {
                console.log('Connected');
            })
            .on('close', () => {
                console.log('Connection closed');
                MaxiProxReader.Close();
            });

        Events.onSetReaderOnOffState.attach(this.SetActive);
        this.client.on('data', MaxiProxReader.OnDataRead);
        
    }

    private static OnDataRead(data) {
        if (!MaxiProxReader.mpr.active)
            return;
        var stringData = data.toString('utf8');
        var newlines = stringData.split('\r\n');
        var parts = newlines.map(x => x.split(':'));
        var id, bitcount, fc, cc, hex;
        parts.forEach((line, index) => {
            switch (line[0].toLowerCase()) {
                case "id": id = line[1]; break;
                case "bitcount": bitcount = line[1]; break;
                case "fc": fc = line[1]; break;
                case "hex": hex = line[1]; break;
            }
        });

        var newCard = RFIDTag.FromString(hex, [], "HID", parseInt(bitcount), fc);
        Logger.LogInfo(JSON.stringify(newCard));
        Events.onRFIDRead.post(newCard);
    }

    public SetActive = (state: boolean) => MaxiProxReader.mpr.active = state;
}
