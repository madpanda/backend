import * as util from "util";
import * as RFIDTag from "../../Models/RFID/RFIDTag";
import { execFile, execFileSync } from "child_process";
import { Settings } from "../../Settings";

import fs = require("fs");
import sleep = require("sleep");
import { Events } from "../../Events";
import { Logger } from "../Logging/Logger";

export class Proxmark {
    private active: boolean;
    private static pm3: Proxmark;
    private static MIN_BLOCK_INDEX: number = 0;
    private static MAX_BLOCK_INDEX: number = 7;
    private static proxmarkPath = Settings.GetSettings("PATH_TO_PROXMARK_APP");
    private static cmdWipe = "lf t55 wipe";
    private static cmdDetect = "lf t55 detect";
    private static cmdClonePrefix = "lf hid clone %s";
    private static hidValidCard = Settings.GetSettings("HID_VALID_CARD");

    private static get proxmarkTty(): string {
        if (Settings.GetSettings("DEBUG"))
            return Settings.GetSettings("PATH_TO_PROXMARK_TTY");

        if (fs.existsSync(Settings.GetSettings("PATH_TO_PROXMARK_TTY"))) {
            Logger.LogInfo("Using normal tty.");
            return Settings.GetSettings("PATH_TO_PROXMARK_TTY");
        } else if (fs.existsSync(Settings.GetSettings("PATH_TO_PROXMARK_TTY_FALLBACK"))) {
            Logger.LogInfo("Using fallback tty.");
            return Settings.GetSettings("PATH_TO_PROXMARK_TTY_FALLBACK");
        } else
            throw Error("Proxmark device is not attached or recognized");
    }

    private static quoteCmd = (cmd: string) => util.format('"%s"', cmd);
    private static prepClone = (cardID: string) => util.format(Proxmark.cmdClonePrefix, cardID);
    private wipeCard = () => Proxmark.pm3.SendCommand(Proxmark.cmdWipe);
    private initCard = () => Proxmark.pm3.SendCommand(Proxmark.prepClone(Proxmark.hidValidCard));
    public GetActive(): boolean { return Proxmark.pm3.active; }
    public SetActive(state: boolean) { Proxmark.pm3.active = state; }

    private constructor() {
        if (Proxmark.proxmarkTty) {
            Events.onRFIDWrite.attach(this.CloneTag);
            Events.onSetWriterOnOffState.attach(this.SetActive);
            Events.onWriteStatusNotify.post("Writer turned on.");
        }
    }

    public static Singleton(setAsActive: boolean = true): Proxmark {
        if (!Proxmark.pm3) Proxmark.pm3 = new Proxmark();
        Proxmark.pm3.SetActive(setAsActive);
        return Proxmark.pm3;
    }

    public Close() {
        if (Proxmark.pm3) {
            Events.onRFIDWrite.detach(this.CloneTag);
            Events.onSetWriterOnOffState.detach(Proxmark.pm3.SetActive);
            Events.onWriteStatusNotify.post("Writer turned off.");
            Proxmark.pm3 = null;
        }
    }

    public ForceCloneTag(tag: RFIDTag.default): boolean {
        var previousState = this.GetActive();

        Logger.LogInfo("Forcing tag clone...");
        Logger.LogWarn("!! THIS FUNCTION MAY INTERFERE WITH OTHER API ACTIONS. DO NOT RUN WHEN RUNNING OTHER ACTIONS, AS THIS MAY LEAD TO UNWANTED RESULTS. !!")

        this.SetActive(true);

        var result = this.CloneTag(tag);

        this.SetActive(previousState);
        
        return result;
    }

    /** Writes a tag with the given uid from an RFID Tag. */
    public CloneTag(tag: RFIDTag.default): boolean {
		var returnString = "";
		if(!Proxmark.pm3.active)
			return;

        Logger.LogInfo("Checking card.");
        var readerOutput = Proxmark.pm3.SendCommand("lf t55 detect");
        var cardFoundIndex = readerOutput.indexOf("Could not detect modulation automatically.");
        if (cardFoundIndex >= 0) {
            Events.onWriteStatusNotify.post("No card detected.");
            Logger.LogError("Clone was invoked, but no writable card was found.");
            return false;
	}
		
        Logger.LogInfo("Wiping card.");
        Proxmark.pm3.wipeCard();

        Logger.LogInfo("Writing HID Settings.");
        Proxmark.pm3.initCard();

        Logger.LogInfo("Writing Card with ID: ", tag.Uid.RawUid);
        Proxmark.pm3.SendCommand(Proxmark.prepClone(tag.Uid.RawUid));

        Logger.LogInfo("Verifying write");
        var readerOutput = Proxmark.pm3.SendCommand("lf search");
        var cardFoundIndex = readerOutput.indexOf("Valid HID Prox ID Found!");
        var cardFoundHex = readerOutput.toString().toLowerCase().indexOf(tag.Uid.RawUid.toLowerCase());
        
        if (cardFoundIndex >= 0 && cardFoundHex >= 0) {
			returnString = "Successfully wrote to the card.";
			Logger.LogInfo(returnString);
        } else if (cardFoundIndex < 0) {
			returnString = "There was an error while writing. Please try again.";
			Logger.LogError(returnString);
        }
        else if (cardFoundHex < 0) {
			returnString = "The write succeeded but the validation check failed.";
			Logger.LogError(returnString);
        }
        
		Events.onWriteStatusNotify.post(returnString);
		
        return cardFoundIndex >= 0 && cardFoundHex >= 0;
    }

    /** UNSAFE! 
     * Overrides an RFID tag with the given tag, writing each and every block. */
    public WriteTag(tag: RFIDTag.default) {
		if(!Proxmark.pm3.active)
			return;
		
        Events.onWriteStatusNotify.post("Tag writer called (unsafe!).");

        Logger.LogInfo(JSON.stringify(tag));
        var uid = tag.Uid.RawUid;
        console.log("Writing Tag with UID: ", uid);
        Proxmark.pm3.WriteBlock(0, Number.parseInt(uid, 16));

        var strBlocks = new Array<string>(tag.Blocks.reduce((prev, cur) => prev.BlockNumber < cur.BlockNumber ? cur : prev).BlockNumber);
        var blocks = tag.Blocks.forEach(x => {
            var y = x.BlockData.substr(0, 4);
            var z = Number.parseInt(y.toString(), 16);
            Proxmark.pm3.WriteBlock(x.BlockNumber, z);
        });
    }

    private SendCommand(cmd: string) {
        if (cmd) {
            var cmdLine = Proxmark.quoteCmd(cmd);
            Logger.LogInfo("Executing Proxmark command: ", cmdLine);
            var cmdProc = execFileSync(Proxmark.proxmarkPath, [Proxmark.proxmarkTty, "-c", cmd]);

            Logger.LogInfo(util.format("Child process output:\n%s", cmdProc));
            return cmdProc;
        } else {
            console.error("SendCommand: command not given");
        }
    }

    private WriteBlock(blockIndex: number, block: number, page: number = 0): void {
        if (blockIndex < Proxmark.MIN_BLOCK_INDEX || blockIndex > Proxmark.MAX_BLOCK_INDEX)
            throw Error(util.format("Block index must be between %s and %s.", Proxmark.MIN_BLOCK_INDEX, Proxmark.MAX_BLOCK_INDEX));

        const cmdPrefix = "lf t55 write b %s d %s %s";
        var blockInput = blockIndex.toString();
        var byteInput = block.toString(16);
        var preppedString = util.format(cmdPrefix, blockInput, byteInput, page.toString());
        this.SendCommand(preppedString);
        //console.log(util.format(cmdPrefix, blockInput, byteInput));
    }
}
