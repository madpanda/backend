import * as child_process from "child_process"
import * as util from "util"
import * as sleeper from "sleep"

export class Proxmark {
    private static MIN_BLOCK_INDEX: number = 0;
    private static MAX_BLOCK_INDEX: number = 7;
    private static pm3: Proxmark;
    private static cmd = "./proxmark";
    private static pipe = ["/dev/ttySCM0"];

    private static childProcess: child_process.ChildProcess;

    private constructor() {

        Proxmark.childProcess = child_process.spawn(Proxmark.cmd, Proxmark.pipe);

        /// Uncomment the following line to pipe  to child process

        //process.stdin.on('data', (data) => {
        //    console.log('process.stdin!');
        //    Proxmark.childProcess.stdin.write(data);
        //});

        Proxmark.childProcess.stdout.on('data', (data) => {
            console.log('child.stdout!');
            process.stdout.write(data);
        });
    }

    public static Singleton(): Proxmark {
        if (!Proxmark.pm3)
            Proxmark.pm3 = new Proxmark();
        return Proxmark.pm3;
    }

    public WriteBlock(blockIndex: number, block: number): void {
        if (blockIndex < Proxmark.MIN_BLOCK_INDEX || blockIndex > Proxmark.MAX_BLOCK_INDEX)
            throw Error(util.format("Block index must be between %s and %s.", Proxmark.MIN_BLOCK_INDEX, Proxmark.MAX_BLOCK_INDEX));

        const cmdPrefix = "lf t55 write b %s d %s \n";
        var blockInput = blockIndex.toString();
        var byteInput = block.toString(16);
        var preppedString = util.format(cmdPrefix, blockInput, byteInput);

        Proxmark.childProcess.stdin.write(preppedString);
        //console.log(util.format(cmdPrefix, blockInput, byteInput));
    }

    

    public async Exit() {
        for (let retries = 0; !Proxmark.childProcess.killed; retries++) {
            Proxmark.childProcess.kill();
            sleeper.msleep(500);
            if (retries >= 5) {
                throw Error("Could not gracefully kill child process. Committing suicide...");
            }
        }
    }
}
