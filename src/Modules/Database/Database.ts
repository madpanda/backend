import { Sequelize } from "sequelize-typescript";
import { RFID_Block } from "./models/RFID_Block"
import { RFID_Tag } from "./models/RFID_Tag"
import { User } from "./models/User"
import { Session } from "./models/Session"
import escape = require("jsesc");
import fs = require("fs");
import {
    DatabaseNotOpenError,
    SessionAlreadyOpenError,
    NoSessionOpenError,
    UserNotLoggedInError,
    UserNotFoundError,
    SessionNotFoundError
} from "./DatabaseErrors";

import csv = require("to-csv");
import RFIDTag from "../../Models/RFID/RFIDTag";
import RFIDUid from "../../Models/RFID/RFIDUid";
import { Logger } from "../Logging/Logger";

export module DatabaseWrapper {

    export class Database {

        private static db: Sequelize;
        public static session: Session;
        public static UserID: number = -1;
        private static dbName: string = "sampledb"; //TODO: place in config
        private static dbFilePath: string = "./sampledb.db"; //TODO: place in config

        /** Opens or creates a SQLite database.
         * Also creates or logins in one user.
         * If a connection is already open, it does nothing.
         */
        public static async Open(dbUnlockPassword: string, dbUsername: string, dbUserpass: string): Promise<boolean> {
            try {
                if (!fs.existsSync(this.dbFilePath)) {
                    await Database.OpenOrCreate(dbUnlockPassword);
                    await Database.CreateUser(dbUsername, dbUserpass);
                } else
                    await Database.OpenOrCreate(dbUnlockPassword);
                await Database.Login(dbUsername, dbUserpass);
                //await Database.StartSession(sessionName);
                return true;
            } catch (err) {
                console.error(err);
                return false;
            }
        }


        /** Checks if a user is logged in. */
        public static CheckUserLoggedIn(): void {
            if (!this.IsOpen())
                throw new DatabaseNotOpenError();
            else if (!this.IsLoggedIn())
                throw new UserNotLoggedInError();
        }

        /**
         * Check is a user is logged in and a session exists.
         * @throws DatabaseNotOpenError
         * @throws UserNotLoggedInError
         * @throws NoSessionOpenError
         */
        public static CheckUserLoggedInSessionExists(): void {
            this.CheckUserLoggedIn();
            if (!this.IsSessionOpen())
                throw new NoSessionOpenError();
        }

        /** Ends the session and closes the SQLite database.
         * If no connection is open, it does nothing.
         */
        public static async Close() {
            if (this.IsSessionOpen())
                await this.EndSession();
            this.UserID = -1;
            this.db = null;
        }

        /** Creates rfid tag and blocks, writes blocks to tag and saves to database.
         * @throws DatabaseNotOpenError
         * @throws UserNotLoggedInError
         * @throws NoSessionOpenError
         */
        public static async WriteTag(rfidtag: RFIDTag): Promise<number> {
            this.CheckUserLoggedInSessionExists();

            const uid = rfidtag.Uid.RawUid;
            const blocks = rfidtag.Blocks;

            // persist tag.
            const tag = new RFID_Tag({ UID: uid, nBlocks: blocks.length, SessionId: this.session.id, DateAdded: new Date(Date.now()), BitCount: rfidtag.BitCount, CardType: rfidtag.CardType });
            await tag.save();

            // iterate over blocks, create a new block for every string and persist block.
            for (let index = 0; index < blocks.length; index++) {
                let element = blocks[index];
                if (!element)
                    continue;
                const block = new RFID_Block({ BlockIndex: element.BlockNumber, RFID_TagId: tag.id, Data: element.BlockData });
                await block.save();
            }

            return tag.id;
        }
        /**
         * Reads a tag from the database, on uid.
         * Returns an empty tag if no tags are found.
         * @throws DatabaseNotOpenError
         * @throws UserNotLoggedInError
         * @throws NoSessionOpenError
         */
        public static async ReadTag(uid: string): Promise<RFIDTag> {
            this.CheckUserLoggedInSessionExists();

            const tag = await RFID_Tag.findOne({
                where: {
                    UID: uid,
                    SessionId: this.session.id
                },
                include: [
                    { model: RFID_Block }
                ]
            });

            return new RFIDTag(new RFIDUid(tag.UID), tag.Blocks.map((x) => x.ToJBlock()), tag.CardType, tag.BitCount);
        }

        public static async ReadTagById(id: number): Promise<RFIDTag> {
            this.CheckUserLoggedInSessionExists();

            var tag = await RFID_Tag.findById(id, {
                include: [
                    { model: RFID_Block }
                ]
            });

            if (!tag) return null;

            return new RFIDTag(new RFIDUid(tag.UID), tag.Blocks.map((x) => x.ToJBlock()), tag.CardType, tag.BitCount);
        }

        public static async ExportAll(filepath: string): Promise<void> {
            await Session.findAndCountAll({
                where: { UserId: this.UserID },
                include: [{ model: RFID_Tag, include: [{ model: RFID_Block }] }]
            }).then((retval) => {
                if (retval.count > 0) {
                    fs.writeFileSync(filepath, csv(retval.rows));
                    console.log("Finished writing csv to %s", __dirname);
                } else {
                    console.log("Export has been called but no results were found.");
                }
            }).catch((err) => {
                if (err) {
                    console.error(err);
                    throw err;
                }
            });
        }

        public static async ListSessions(): Promise<any> {
            this.CheckUserLoggedIn();

            Logger.LogInfo(this.UserID.toString());

            return (await Session.findAll({ where: { UserId: this.UserID } })).map((session) => {
                return {
                    Id: session.id,
                    Name: session.Name,
                    AuditDate: session.StartDate
                }
            });
        }

        /** Deletes the database. No recovery possible! */
        public static async Delete(): Promise<void> {
            if (Database.Exists())
                await fs.unlink(this.dbFilePath, (err) => { if (err) throw err; });
        }

        public static Exists(): boolean {
            return fs.existsSync(this.dbFilePath);
        }

        private static async OpenOrCreate(unlockPassword: string) {

            //TODO: verify necessity
            //dbfilepath = escape(dbfilepath); 
            //username = escape(username);
            //passwordHash = escape(passwordHash);
            if (!this.IsOpen())
                this.db = new Sequelize({
                    database: Database.dbName,
                    dialect: 'sqlite',
                    username: 'root',
                    password: unlockPassword,
                    storage: this.dbFilePath,
                    modelPaths: [__dirname + '/models']
                });
            await this.db.sync();
        }

        private static async CreateUser(username: string, passwordHash: string) {
            username = escape(username);
            passwordHash = escape(passwordHash);

            var user = new User({ Username: username, Password: passwordHash });
            await user.save();
        }

        private static async Login(username: string, passwordHash: string) {
            // Finds a user with a given username and password 
            username = escape(username);
            passwordHash = escape(passwordHash);

            const find_result = await User.findOne({ where: { Username: username, Password: passwordHash } });
            if (find_result && find_result.id)
                this.UserID = find_result.id;
            else {
                this.UserID = -1;
                throw new UserNotFoundError();
            }
        }

        public static async CreateSession(name: string): Promise<number> {
            if (!this.IsOpen())
                throw new DatabaseNotOpenError();

            name = escape(name);
            var session = new Session({ Name: name, StartDate: new Date(Date.now()), UserId: this.UserID });
            await session.save();
            return session.id;
        }

        public static async DeleteSession(sessionId: number) {
            if (!this.IsOpen())
                throw new DatabaseNotOpenError();

            var session = await Session.findById(sessionId);

            if (session) {
                await session.destroy();
            } else {
                throw new SessionNotFoundError();
            }
        }

        public static async GetSession(sessionId: number): Promise<Session> {
            if (!this.IsOpen())
                throw new DatabaseNotOpenError();

            var session = await Session.findById(sessionId, {
                include: [{ model: RFID_Tag, include: [{ model: RFID_Block }] }]
            });

            return session;
        }

        public static async OpenSession(sessionId: number) {
            if (!this.IsOpen())
                throw new DatabaseNotOpenError();

            if (this.session)
                throw new SessionAlreadyOpenError();

            var session = await Session.findById(sessionId);

            if (session) {
                this.session = session;
            } else {
                throw new SessionNotFoundError();
            }
        }

        public static async StartSession(name: string) {
            if (!this.IsOpen())
                throw new DatabaseNotOpenError();

            if (this.session)
                throw new SessionAlreadyOpenError();

            name = escape(name);
            this.session = new Session({ Name: name, StartDate: new Date(Date.now()), UserID: this.UserID });
            await this.session.save();
        }

        public static async EndSession() {
            this.CheckUserLoggedInSessionExists();

            await this.session.update("EndDate", new Date(Date.now()));
            this.session = null;
        }

        public static async EndSessionForced() {
            if(this.IsSessionOpen()) {
                await this.EndSession();
            } else {
                this.CheckUserLoggedIn();

                this.session = null;
            }
        }

        private static IsOpen(): boolean {
            return this.db != null && this.db.isDefined("User");
        }

        private static IsLoggedIn(): boolean {
            return this.UserID >= 0;
        }

        private static IsSessionOpen(): boolean {
            return this.session != null;
        }

        public static LoginUser(username: string, passwordHash: string): boolean {
            username = escape(username);
            passwordHash = escape(passwordHash);
            return User.findOne({ where: { Username: username, Password: passwordHash } }) ? true : false;
        }

        public static async ExportAllSessions(): Promise<string[]> {
            var res = ["No results"];
            await Session.findAndCountAll({
                // where: { UserId: this.userID },
                include: [{ model: RFID_Tag, include: [{ model: RFID_Block }] }]
            }).then((retval) => {
                if (retval.count > 0) {
                    res = csv(retval.rows.map(function (item) { return { User_Id: item.UserId, id: item.id, Tags: item.Tags.map(function (tag) { return tag.UID }), Created_at: item.StartDate }; }));
                } else {
                    console.log("Export has been called but no results were found.");
                }
            }).catch((err) => {
                if (err) {
                    console.error(err);
                    throw err;
                }
            });

            return res;
        }

        public static async ExportSession(sessionId: string): Promise<RFIDTag[]> {
            this.CheckUserLoggedIn();
            var result = null;

            var session = await Session.findById(sessionId, {
                include: [{ model: RFID_Tag, include: [{ model: RFID_Block }]  }] 
            });

            if(session && session.Tags) {
                result = csv({ User_Id: session.UserId, id: session.id, Tags: session.Tags.map(function (tag) { return tag.UID }), Created_at: session.StartDate });
            } else {
                return [];
            }
            return result;
        }

    }
}