export class DatabaseNotOpenError extends Error {
    constructor() {
        super("Database is not open. Please open a database connecting using the 'Database.Open' function.");
    }
}
export class SessionNotFoundError extends Error {
    constructor() {
        super("Session not found.");
    }
}

export class SessionAlreadyOpenError extends Error {
    constructor() {
        super("Session is already open. Please close it using the 'Database.EndSession' function.");
    }
}
export class NoSessionOpenError extends Error {
    constructor() {
        super("No sessions are open right now. Please open one using the 'Database.StartSession' function.");
    }
}
export class UserNotFoundError extends Error {
    constructor() {
        super("No user has been found with this username/password combination. Please try again.");
    }
}
export class UserNotLoggedInError extends Error {
    constructor() {
        super("No user has been found with this username/password combination. Please try again.");
    }
}