import { Table, Column, Model, DataType, ForeignKey, IsDate,HasMany } from "sequelize-typescript"
import { User } from "./User"
import { RFID_Tag } from './RFID_Tag';

@Table
export class Session extends Model<Session> {

    @Column(DataType.TEXT)
    Name: string;

    @Column(DataType.INTEGER)
    @ForeignKey(() => User)
    UserId: number;

    @IsDate
    @Column(DataType.DATE)
    StartDate: Date;

    @IsDate
    @Column(DataType.DATE)
    EndDate: Date;

    @HasMany(() => RFID_Tag)
    Tags: RFID_Tag[];
}
