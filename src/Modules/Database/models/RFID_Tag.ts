import { Table, Column, Model, DataType, HasMany, BelongsTo, ForeignKey } from "sequelize-typescript";
import { RFID_Block } from "./RFID_Block"
import { Session } from "./Session"
import RFIDTag from "../../../Models/RFID/RFIDTag";
import RFIDUid from "../../../Models/RFID/RFIDUid";

@Table
export class RFID_Tag extends Model<RFID_Tag> {

    @Column(DataType.TEXT)
    UID: string;

    @HasMany(() => RFID_Block)
    Blocks: RFID_Block[];

    @Column(DataType.INTEGER)
    nBlocks: number;

    @BelongsTo(() => Session, "id")
    Session: Session;

    @ForeignKey(() => Session)
    SessionId: number;

    @Column(DataType.DATE)
    DateAdded: Date;

    @Column(DataType.TEXT)
    CardType: string;

    @Column(DataType.INTEGER)
    BitCount: number;

    public ToJTag(): RFIDTag {
        return new RFIDTag(new RFIDUid(this.UID), this.Blocks.map((v) => v.ToJBlock()), this.CardType, this.BitCount)
    }
}