import { Table, Column, Model, DataType } from "sequelize-typescript";

@Table
export class User extends Model<User> {

    @Column(DataType.TEXT)
    Username: string;

    @Column(DataType.TEXT)
    Password: string;

}