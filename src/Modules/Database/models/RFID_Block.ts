import { Table, Column, Model, DataType, BelongsTo, ForeignKey } from "sequelize-typescript"
import { RFID_Tag } from "./RFID_Tag"
import RFIDDataBlock from "../../../Models/RFID/RFIDDataBlock";

@Table
export class RFID_Block extends Model<RFID_Block> {

    @BelongsTo(() => RFID_Tag)
    RFID_Tag: RFID_Tag;

    @ForeignKey(() => RFID_Tag)
    RFID_TagId: number;

    @Column(DataType.INTEGER)
    BlockIndex: number;

    @Column(DataType.TEXT)
    Data: string;

    public ToJBlock(): RFIDDataBlock {
        return new RFIDDataBlock(this.BlockIndex, this.Data);
    }
}