export default class RFIDUid {
    public readonly RawUid: string;
    public readonly AsInt: number;
    public readonly IsValid: boolean;

    /** Needs a hex formatted string*/
    constructor(rawUid: string) {
        this.RawUid = rawUid;
        this.AsInt = Number.parseInt(rawUid, 16);
        this.IsValid = this.AsInt != 0;
    }
}