export default class RFIDDataBlock {
    public readonly BlockNumber: number;
    public readonly BlockData: string;

    constructor(blockNumber: number, blockData: string) {
        this.BlockNumber = blockNumber;
        this.BlockData = blockData;
    }
}