import RFIDDataBlock from "./RFIDDataBlock";
import RFIDUid from "./RFIDUid";

export default class RFIDTag {
    public readonly FacilityCode: number;
    public readonly Uid: RFIDUid;
    public readonly Blocks: RFIDDataBlock[];
    public readonly CardType: string;
    public readonly BitCount: number;

    constructor(uid: RFIDUid, blocks: RFIDDataBlock[], cardType: string, bitCount: number, fc?:number) {
        this.Uid = uid;
        this.CardType = cardType;
        this.BitCount = bitCount;
        this.FacilityCode = fc;
        this.Blocks = blocks;
    }

    public static FromString(uid:string, blocks:string[], cardType: string, bitCount: number, fc?:string):RFIDTag{
        return new RFIDTag(
            new RFIDUid(uid),
            blocks.map( (v,i) => new RFIDDataBlock(i, v)),
            cardType,
            bitCount,
            Number.parseInt(fc)
        );
    }
}
