// Dependencies
import express = require("express");
import * as longpoll from "express-longpoll";
import bodyparser = require("body-parser");
import commander = require("commander");
import jsonwebtoken = require("jsonwebtoken");
import express_async_await = require("express-async-await");
var jwt = require("express-jwt");

// Modules
import { Logger } from "./Modules/Logging/Logger";
import { DatabaseWrapper } from "./Modules/Database/Database"

// Other
import { Events } from "./Events";
import { ConsoleAuth } from "./Modules/Authentication/consoleAuth"
import * as child_process from "child_process";
import { ShutdownModule } from "./Modules/Shutdown/ShutdownModule";
import { SessionNotFoundError, UserNotFoundError } from "./Modules/Database/DatabaseErrors";
import * as fs from "fs";
import * as http from "http";
import * as https from "https";
import * as crypto from "crypto";

var privateKey = fs.readFileSync('sslcert/private.key', 'utf8');
var certificate = fs.readFileSync('sslcert/server.crt', 'utf8');
var credentials = { key: privateKey, cert: certificate, passphrase: "madpanda" };

const Database = DatabaseWrapper.Database;

import { MaxiProxReader } from "./Modules/Readers/MaxiProxReader"
import { Proxmark } from "./Modules/Writers/Proxmark"
import RFIDTag from "./Models/RFID/RFIDTag";
import { Settings } from "./Settings";

var pm = Proxmark.Singleton(false);
var mpr = MaxiProxReader.Singleton();


// ExitHook
const exitHook = require('exit-hook');
exitHook(() => {
    Database.Close();
});

enum SystemState {
    None,
    Reading,
    Writing,
    Idle,
    SystemError
}

enum StatusCode {
    Ok,
    Error,
}

class ResponseModel {
    Code: StatusCode;
    Data: any;
    ErrorMessage: string;

    constructor(code: StatusCode, data: any = null) {
        this.Code = code;
        this.Data = data;
    }
}

class ErrorResponseModel extends ResponseModel {
    constructor(msg: string) {
        super(StatusCode.Error);

        this.ErrorMessage = msg;
    }
}

const jwtKeyFile = "jwt.key";

var jwt_sec: string;

if (fs.existsSync(jwtKeyFile)) {
    jwt_sec = fs.readFileSync(jwtKeyFile).toString();
} else {
    jwt_sec = crypto.randomBytes(256).toString("hex");

    fs.writeFileSync(jwtKeyFile, jwt_sec);
}

function SetHardwareStates(enableReader: boolean, enableWriter: boolean) {
    Events.onSetReaderOnOffState.post(enableReader);
    Events.onSetWriterOnOffState.post(enableWriter);
}

function CheckAuth(resp): boolean {
    try {
        Database.CheckUserLoggedIn()
    } catch (exception) {
		resp.writeHead(401); // HTTP 401 Unauthorized
        resp.end(JSON.stringify(new ErrorResponseModel("Invalid auth")));
        return false;
    }
    return true;
}

function Main() {
    var app = express_async_await(express());
    var lp = longpoll(app);

    app.use(jwt(
        { secret: jwt_sec, }
    ).unless({
        path: ["/api/login", "/api/register", "/api/initialized", /\/web\/*/, "/"],
    }));

    app.use('/api/:area/:page', async function (_, resp, next) {
        if (!CheckAuth(resp))
            return;
        next();
    });

    app.get("/", (req, resp) => {
        resp.redirect("/web");
    });

    app.use("/web", express.static("web"))
        .use(bodyparser.json());

    app.get("/api/initialized", (req, resp) => {
        resp.send(new ResponseModel(StatusCode.Ok, Database.Exists()));
    });

    app.post("/api/login", (req, resp) => {
        if (!req.body || !req.body.username || !req.body.password) {
            resp.send(new ErrorResponseModel(new UserNotFoundError().message));
            return;
        }

        try {
            Database.CheckUserLoggedInSessionExists();
            if (Database.LoginUser(req.body.username, req.body.password)) {
                resp.send(new ResponseModel(StatusCode.Ok, jsonwebtoken.sign({ id: Database.UserID }, jwt_sec)))
                return;
            }
            resp.writeHead(302);
            resp.end();
            return;
        }
        catch (exception) {
            Database.Open("password", req.body.username, req.body.password)
                .catch(console.error)
                .then(result => {
                    if (result) {
                        resp.send(new ResponseModel(StatusCode.Ok, jsonwebtoken.sign({ id: Database.UserID }, jwt_sec)))
                    } else {
                        resp.send(new ErrorResponseModel(new UserNotFoundError().message));
                    }
                });
        }
    });

    app.get("/api/logout", (_, resp) => {
        try {
            Database.CheckUserLoggedInSessionExists();
            SetHardwareStates(false, false);
            Database.Close();

            resp.send(new ResponseModel(StatusCode.Ok, "Logout successfull"))
        } catch (exception) {
            resp.writeHead(200);
            resp.end();
        }
    });

    app.post("/api/register", (req, resp) => {
        if (!req.body || !req.body.username || !req.body.password || !req.body.repeatPassword) {
            resp.send(new ErrorResponseModel("Invalid request"));
            return;
        }

        if (req.body.password != req.body.repeatPassword) {
            resp.send(new ErrorResponseModel("Passwords do not match."))
            return;
        }

        Database.Open("password", req.body.username, req.body.password)
            .catch(console.error)
            .then(() => {
                try {
                    Database.CheckUserLoggedIn();
                    // SetHardwareStates(false,true);
                    resp.send(new ResponseModel(StatusCode.Ok, jsonwebtoken.sign({ id: Database.UserID }, jwt_sec)))
                }
                catch (exception) {
                    resp.send(new ErrorResponseModel(new UserNotFoundError().message));
                }
            });
    });

    app.get("/api/export/all", (_, resp) => {
        Database.ExportAllSessions().then((csv) => {
            resp.setHeader('Content-disposition', 'attachment; filename=export.csv');
            resp.set('Content-Type', 'text/csv');
            resp.status(200).send(csv);
        });
    });

    app.post("/api/control/export", (req, resp) => {
        if (!req.body || !req.body.sessionid || isNaN(parseInt(req.body.sessionid))) {
            resp.send(new ErrorResponseModel("Invalid request"));
            return;
        }

        Database.ExportSession(req.body.sessionid).then((csv) => {
            resp.setHeader('Content-disposition', 'attachment; filename=export.csv');
            resp.set('Content-Type', 'text/csv');
            resp.status(200).send(csv);
        });
    });

    app.get("/api/control/shutdown", (req, resp) => {
        resp.send(new ResponseModel(StatusCode.Ok));

        ShutdownModule.Shutdown();
    });

    app.get("/api/control/sessions", async (req, resp) => {
        resp.send(new ResponseModel(
            StatusCode.Ok,
            {
                Sessions: await Database.ListSessions()
            }
        ));
    });

    app.post("/api/control/createsession", async (req, resp) => {
        if (!req.body || !req.body.sessionname) {
            var responseModel = new ResponseModel(StatusCode.Error);
            responseModel.ErrorMessage = "Invalid request";

            resp.send(responseModel);
            return;
        }

        var sessionName = req.body.sessionname;

        var sessionId = await Database.CreateSession(sessionName);

        resp.send(new ResponseModel(
            StatusCode.Ok,
            {
                SessionId: sessionId
            }
        ));
    });

    app.post("/api/control/deletesession", async (req, resp) => {
        if (!req.body || !req.body.sessionid || isNaN(parseInt(req.body.sessionid))) {
            var responseModel = new ResponseModel(StatusCode.Error);
            responseModel.ErrorMessage = "Invalid request";

            resp.send(responseModel);
            return;
        }

        try {
            await Database.DeleteSession(req.body.sessionid);
            resp.send(new ResponseModel(StatusCode.Ok));
        } catch (e) {
            var responseModel = new ResponseModel(StatusCode.Error);
            responseModel.ErrorMessage = e.message;

            resp.send(responseModel);
        }
    });



    app.post("/api/control/updatesession", async (req, resp) => {
        if (!req.body || !req.body.sessionid || !req.body.sessionname || isNaN(parseInt(req.body.sessionid))) {
            var responseModel = new ResponseModel(StatusCode.Error);
            responseModel.ErrorMessage = "Invalid request";

            resp.send(responseModel);
            return;
        }

        var session = await Database.GetSession(req.body.sessionid);

        if (session) {
            session.Name = req.body.sessionname;
            await session.save();

            resp.send(new ResponseModel(StatusCode.Ok));
        } else {
            var responseModel = new ResponseModel(StatusCode.Error);
            responseModel.ErrorMessage = new SessionNotFoundError().message;

            resp.send(responseModel);
        }
    });

    var currentSystemState: SystemState = SystemState.Idle;

    app.get("/api/control/getsystemstate", (req, resp) => {
        resp.send(new ResponseModel(StatusCode.Ok, {
            State: currentSystemState
        }));
    });

    var runningSessionId = null;

    app.post("/api/control/sessiontags", async (req, resp) => {
        if (!req.body || !req.body.sessionid || isNaN(parseInt(req.body.sessionid))) {
            var responseModel = new ResponseModel(StatusCode.Error);
            responseModel.ErrorMessage = "Invalid request";

            resp.send(responseModel);
            return;
        }

        var sessionId = req.body.sessionid;

        var session = await Database.GetSession(sessionId);

        if (session) {
            resp.send(new ResponseModel(StatusCode.Ok, session.Tags.map(tag => {
                return {
                    id: tag.id,
                    tag: {
                        tagUid: tag.UID,
                        cardType: tag.CardType,
                        bitCount: tag.BitCount,
                        dateAdded: tag.DateAdded
                    }
                };
            })));
        } else {
            var responseModel = new ResponseModel(StatusCode.Error);
            responseModel.ErrorMessage = new SessionNotFoundError().message;

            resp.send(responseModel);
        }
    });

    app.post("/api/control/setsystemstate", (req, resp) => {
        if (!req.body || !req.body.state) {
            var responseModel = new ResponseModel(StatusCode.Error);
            responseModel.ErrorMessage = "Invalid request";

            resp.send(responseModel);
            return;
        }

        if (!runningSessionId) {
            var responseModel = new ResponseModel(StatusCode.Error);
            responseModel.ErrorMessage = "No session open";

            resp.send(responseModel);
            return;
        }

        var state: SystemState = <SystemState>SystemState[<string>req.body.state.toString()];

        if (state) {
            currentSystemState = state;

            Events.onTerminateAllReaderActions.post({});
            Events.onTerminateAllWriterActions.post({});

            switch (state) {
                case SystemState.Idle:
                    Events.onSetReaderOnOffState.post(false);
                    Events.onSetWriterOnOffState.post(false);
                    break;

                case SystemState.Reading:
                    Events.onSetReaderOnOffState.post(true);
                    Events.onSetWriterOnOffState.post(false);
                    break;

                case SystemState.Writing:
                    Events.onSetReaderOnOffState.post(false);
                    Events.onSetWriterOnOffState.post(true);
                    break;

                case SystemState.SystemError:
                    Events.onSetReaderOnOffState.post(false);
                    Events.onSetWriterOnOffState.post(false);
                    break;
            }

            resp.send(new ResponseModel(StatusCode.Ok));
        } else {
            var responseModel = new ResponseModel(StatusCode.Error);
            responseModel.ErrorMessage = "Invalid request";

            resp.send(responseModel);
        }
    });

    async function StartSession(sessionId: number): Promise<boolean> {
        if (runningSessionId) {
            if (!await EndSession()) {
                return false;
            }
        }

        var session = await Database.GetSession(sessionId);

        if (session) {
            if (!session.StartDate) {
                session.StartDate = new Date(Date.now());
                session.save();
            }

            runningSessionId = sessionId;

            Events.onTerminateAllReaderActions.post(null);
            Events.onTerminateAllWriterActions.post(null);
            Events.onSetReaderOnOffState.post(false);
            Events.onSetWriterOnOffState.post(false);

            await Database.OpenSession(sessionId);

            return true;
        } else {
            return false;
        }
    }

    async function EndSession(): Promise<boolean> {
        if (!runningSessionId) {
            return false;
        }

        var session = await Database.GetSession(runningSessionId);

        if (session) {
            session.EndDate = new Date(Date.now());
            await session.save();

            runningSessionId = null;

            Events.onTerminateAllReaderActions.post(null);
            Events.onTerminateAllWriterActions.post(null);
            Events.onSetReaderOnOffState.post(false);
            Events.onSetWriterOnOffState.post(false);

            await Database.EndSession();

        } else {
            await Database.EndSessionForced();
        }

        return true;
    }

    app.post("/api/control/startsession", async (req, resp) => {
        if (!req.body || !req.body.sessionid || isNaN(parseInt(req.body.sessionid))) {
            var responseModel = new ResponseModel(StatusCode.Error);
            responseModel.ErrorMessage = "Invalid request";

            resp.send(responseModel);
            return;
        }

        var sessionId = req.body.sessionid;

        if (await StartSession(parseInt(sessionId))) {
            resp.send(new ResponseModel(StatusCode.Ok));
        } else {
            var responseModel = new ResponseModel(StatusCode.Error);
            responseModel.ErrorMessage = new SessionNotFoundError().message;

            resp.send(responseModel);
        }
    });

    app.post("/api/control/endsession", async (req, resp) => {
        if (await EndSession()) {
            resp.send(new ResponseModel(StatusCode.Ok));
        } else {
            var responseModel = new ResponseModel(StatusCode.Error);
            responseModel.ErrorMessage = "Could not end session";

            resp.send(responseModel);
        }
    });

    lp.create("/api/control/read");

    Events.onRFIDRead.attach(tag => {
        if (!runningSessionId) {
            return;
        }

        if (!tag.Uid.IsValid) {
            return;
        }

        Database.WriteTag(tag).then(tagId => {
            lp.publish("/api/control/read", new ResponseModel(StatusCode.Ok, {
                id: tagId,
                tag: {
                    tagUid: tag.Uid.RawUid,
                    cardType: tag.CardType,
                    bitCount: tag.BitCount,
                    dateAdded: new Date(Date.now())
                }
            }));
        });
    });

    app.post("/api/control/write", async (req, resp) => {
        if (!req.body || !req.body.tagid || isNaN(parseInt(req.body.tagid))) {
            var responseModel = new ResponseModel(StatusCode.Error);
            responseModel.ErrorMessage = "Invalid request";

            resp.send(responseModel);
            return;
        }

        var tag = await Database.ReadTagById(req.body.tagid);

        if (tag) {
            Events.onRFIDWrite.post(tag);

            resp.send(new ResponseModel(StatusCode.Ok));
        } else {
            var responseModel = new ResponseModel(StatusCode.Error);
            responseModel.ErrorMessage = "Could not find tag";

            resp.send(responseModel);
        }
    });

    app.post("/api/control/forcewrite", async (req, resp) => {
        function isHex(h: string) {
            var a = parseInt(h, 16);

            var l = padLeft(a.toString(16), "0", 8);
            var r = padLeft(l.toLowerCase(), "0", 8);

            return l === r;
        }

        function padLeft(text: string, padChar: string, size: number) {
            var l = "";

            while(l.length < size - text.length) l += padChar;

            return l + text;
        }

        if (!req.body || !req.body.taguid || !isHex(req.body.taguid)) {
            var responseModel = new ResponseModel(StatusCode.Error);
            responseModel.ErrorMessage = "Invalid request";

            resp.send(responseModel);
            return;
        }

        var uid = padLeft(req.body.taguid.toString(), "0", 8);

        var newCard = RFIDTag.FromString(uid, [], "DynamicForceWriteCard", -1, "-1");

        var result = pm.ForceCloneTag(newCard);

        resp.send(new ResponseModel(StatusCode.Ok, result));
    });

    lp.create("/api/control/writestatus");
    Events.onWriteStatusNotify.attach(msg => {
        lp.publish("/api/control/writestatus", new ResponseModel(StatusCode.Ok, msg));
    });

    var httpServer = http.createServer(app);
    var httpsServer = https.createServer(credentials, app);

    // httpServer.listen(3001);
     httpsServer.listen(443);

    // app.listen(3001, () => {
    //     Logger.LogInfo("Listening on port 3001.");
    // });

}

Main();
