import { SyncEvent } from "ts-events";
import RFIDTag from "./Models/RFID/RFIDTag";
import { Logger } from "./Modules/Logging/Logger";

export namespace Events {
    //OnOffState
    export const onSetReaderOnOffState = new SyncEvent<boolean>();
    onSetReaderOnOffState.attach(isReaderOn => Logger.LogInfo(`Reader OnOffState set to ${isReaderOn}.`));

    export const onSetWriterOnOffState = new SyncEvent<boolean>();
    onSetWriterOnOffState.attach(isWriterOn => Logger.LogInfo(`Writer OnOffState set to ${isWriterOn}.`));

    export const onTerminateAllReaderActions = new SyncEvent();
    onTerminateAllReaderActions.attach(() => Logger.LogInfo(`Terminating all Reader actions.`));

    export const onTerminateAllWriterActions = new SyncEvent();
    onTerminateAllWriterActions.attach(() => Logger.LogInfo(`Terminating all Writer actions.`));

    //StatusChanged
    export const onReaderStatusChanged = new SyncEvent<string>();
    onReaderStatusChanged.attach(status => Logger.LogInfo(`Reader status changed to '${status}'.`));

    export const onWriterStatusChanged = new SyncEvent<string>();
    onWriterStatusChanged.attach(status => Logger.LogInfo(`Writer status changed to '${status}'.`));

    export const onUpdateSystemStatus = new SyncEvent<string>();
    onUpdateSystemStatus.attach(status => Logger.LogInfo(`System status updated to '${status}'.`));

    //ReadWrite
    export const onRFIDRead = new SyncEvent<RFIDTag>();
    onRFIDRead.attach(tag => Logger.LogInfo(`Read RFID Tag '${tag.Uid.RawUid}' of type ${tag.CardType} with ${tag.BitCount} bits.`));

    export const onRFIDWrite = new SyncEvent<RFIDTag>();
    onRFIDWrite.attach(tag => Logger.LogInfo(`Requested to write RFID Tag '${tag.Uid.RawUid}'.`));

    //Notify
    export const onWriteStatusNotify = new SyncEvent<string>();
    onWriteStatusNotify.attach(status => Logger.LogInfo(`Notifying write status '${status}'.`));
}

