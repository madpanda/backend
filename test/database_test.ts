//Unit testing script for Database.ts
/// <reference path="../node_modules/mocha-typescript/globals.d.ts" />
//// <reference path="../node_modules/@types/mocha/index.d.ts" />

import { DatabaseWrapper as dbw } from "../src/Modules/Database/Database";
import { suite, test } from "mocha-typescript"
import { assert } from "chai";
import 'mocha'
import RFIDTag from "../src/Models/RFID/RFIDTag";
import { randomBytes } from "crypto";

@suite()
export class DatabaseTest {

    unlockPassword = "";
    username = "root";
    password = "";
    sampleTag = RFIDTag.FromString("sampleuid", ["sampleblock1", "sampleblock2"], "FakeCardType", 1234);
    export_filepath = "./export.csv";


    @test RightPath() {
        describe("Removing existing database,", () => {
            it("Should delete the existing datebase file.", (done) => {
                dbw.Database.Delete().then(done).catch(assert.fail);
            });
        });

        describe("Opening and writing.", () => {
            it("Should open the database for reading and writing.", (done) => {
                dbw.Database.Open(this.unlockPassword, this.username, this.password).then(x => done(!x)).catch(assert.fail);
            });

            it("Should write some data.", (done) => {
                dbw.Database.WriteTag(this.sampleTag).then(done).catch(assert.fail);
            });
        });

        describe("Closing up.", () => {
            it("Should close the database.", (done) => {
                dbw.Database.Close().then(done).catch(assert.fail);
            });
        });
    }
}