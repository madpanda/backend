#!/usr/bin/python
""" Module for testing input pipe to proxmark app """

import sys

if len(sys.argv) != 4:
    print "ERROR"
    print "Wrong amount of arguments."
elif sys.argv[2] != "-c":
    print "ERROR"
    print "-c option missing"
else:
    print "SUCCESS"

print "Arguments passed:"
print sys.argv
