import shutil
import os
import sys

current_dir = os.getcwd()

last_dir_name = os.path.basename(current_dir)

if not last_dir_name == "build":
    sys.exit("Something went terribly wrong!")

target_dir = os.path.join(os.path.dirname(os.getcwd()), 'dist', 'build')

if os.path.exists(target_dir):
    shutil.rmtree(target_dir)

shutil.copytree(current_dir, target_dir)